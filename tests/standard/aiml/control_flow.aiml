<?xml version="1.0"?>
<aiml>

    <!-- make sure udc.aiml is loaded -->
    <!-- "some crazy random string" / "default" -->

    <!-- - - - - - - - - - - - - - - - - - - - -->
    <!--            R E C U R S I O N          -->
    <!-- - - - - - - - - - - - - - - - - - - - -->

    <!-- "RECURSION A" / "A" -->
    <category>
        <pattern>RECURSION A</pattern>
        <template>A</template>
    </category>

    <!-- "recursion B" / "B" -->
    <category>
        <pattern>RECURSION B</pattern>
        <template>B</template>
    </category>

    <!-- "test srai simple" / "A B" -->
    <category>
        <pattern>TEST SRAI SIMPLE</pattern>
        <template>
            <srai>RECURSION A</srai>
            <srai>recursion b</srai>
        </template>
    </category>

    <!-- "test srai arbitrary RECURSION B" / "B" -->
    <!-- "test srai arbitrary RECURSION A" / "A" -->
    <!-- "test srai arbitrary test srai simple" / "A B" -->
    <category>
        <pattern>TEST SRAI ARBITRARY *</pattern>
        <template><sr/></template>
    </category>

    <!-- - - - - - - - - - - - - - - - - - - - -->
    <!--              R A N D O M              -->
    <!-- - - - - - - - - - - - - - - - - - - - -->

    <!-- "test random simple" / "option a" -->
    <category>
        <pattern>TEST RANDOM SIMPLE</pattern>
        <template>
            <random>
                <li>option a</li>
            </random>
        </template>
    </category>

    <!-- - - - - - - - - - - - - - - - - - - - -->
    <!--               T H I N K               -->
    <!-- - - - - - - - - - - - - - - - - - - - -->

    <!-- "test think simple" / "A" -->
    <category>
        <pattern>TEST THINK SIMPLE</pattern>
        <template>A<think></think></template>
    </category>

    <!-- "test think simple2" / "AC" -->
    <category>
        <pattern>TEST THINK SIMPLE2</pattern>
        <template>A<think>B</think>C</template>
    </category>

    <!-- "test think complex" / "Before after" -->
    <category>
        <pattern>TEST THINK COMPLEX</pattern>
        <template>
            Before<think>
                <lowercase>Some text</lowercase>
                <date/>
                <id/>
            </think> after
         </template>
    </category>

    <!-- "test think complex1" / "" -->
    <category>
        <pattern>TEST THINK COMPLEX1</pattern>
        <template>
           <think>Hello</think>
        </template>
    </category>

    <!-- "test think complex2" / "A C F" -->
    <category>
        <pattern>TEST THINK COMPLEX2</pattern>
        <template>
           A<think>B</think> C<think> D<think>E</think>
               <set name="pred1">val</set>
               <get name="pred1"/>
           </think> F
        </template>
    </category>


    <!-- "test think complex3" / "val1 val2 val3 val2" -->
    <!-- assuming get and set work -->
    <category>
        <pattern>TEST THINK COMPLEX3</pattern>
        <template>
           <set var="var">val1</set>
           <get var="var"/>
           <set name="pred">val2</set>
           <get name="pred"/>

           <think>
              <get var="var"/>
              <get name="pred"/>
              <set var="var">val3</set>
              <get var="var"/>
              <get name="pred"/>
           </think>

           <get var="var"/>
           <get name="pred"/>
        </template>
    </category>

    <!-- - - - - - - - - - - - - - - - - - - - -->
    <!--           C O N D I T I O N           -->
    <!-- - - - - - - - - - - - - - - - - - - - -->
    <!-- assuming set, get and think work as expected -->


    <!-- from https://github.com/logicmoo/programk/blob/master/aiml/test_suite/ProgramY/condition_tests/condition.aiml -->
    <!-- "TEST condition TYPE1 VARIANT1" / "Y" -->
    <category>
		<pattern>TEST CONDITION TYPE1 VARIANT1</pattern>
		<template>
		    <think><set name="var1">value2</set></think>
		    <condition name="var1" value="value1">X</condition>
		    <condition name="var1" value="value2">Y</condition>
		</template>
	</category>

    <!-- "TEST condition TYPE1 VARIANT2" / "Y" -->
	<category>
		<pattern>TEST CONDITION TYPE1 VARIANT2</pattern>
		<template>
		    <think><set name="var1">value2</set></think>
		    <condition name="var1"><value>value1</value>X</condition>
		    <condition name="var1"><value>value2</value>Y</condition>
		</template>
	</category>

    <!-- "TEST condition TYPE1 VARIANT3" / "Y" -->
	<category>
		<pattern>TEST CONDITION TYPE1 VARIANT3</pattern>
		<template>
		    <think><set name="var1">value2</set></think>
		    <condition value="value1"><name>var1</name>X</condition>
		    <condition value="value2"><name>var1</name>Y</condition>
		</template>
	</category>

    <!-- "TEST condition TYPE1 VARIANT4" / "Y" -->
	<category>
		<pattern>TEST CONDITION TYPE1 VARIANT4</pattern>
		<template>
		    <think><set name="var1">value2</set></think>
		    <condition><name>var1</name><value>value1</value>X</condition>
		    <condition><name>var1</name><value>value2</value>Y</condition>
		</template>
	</category>

    <!-- "TEST condition TYPE1 NO MATCH" / "" -->
	<category>
		<pattern>TEST CONDITION TYPE1 NO MATCH</pattern>
		<template>
		    <think><set name="var1">XXX</set></think>
		    <condition name="var1" value="value1">X</condition>
		    <condition name="var1" value="value2">Y</condition>
		</template>
	</category>

    <!-- "TEST condition TYPE2 VARIANT1 WITH DEFAULT" / "DEF" -->
	<category>
		<pattern>TEST CONDITION TYPE2 VARIANT1 WITH DEFAULT</pattern>
		<template>
		    <think><set name="var1">XXX</set></think>
		    <condition name="var1">
		        <li value="value1">X</li>
		        <li value="value2">Y</li>
		        <li value="value3">Z</li>
		        <li>DEF</li>
		    </condition>
		</template>
	</category>

    <!-- "TEST condition TYPE2 VARIANT2 NO DEFAULT" / "Y" -->
	<category>
		<pattern>TEST CONDITION TYPE2 VARIANT2 NO DEFAULT</pattern>
		<template>
		    <think><set name="var1">value2</set></think>
		    <condition name="var1">
		        <li> <value>value1</value>X</li>
		        <li> <value>value2</value>Y</li>
		        <li> <value>value3</value>Z</li>
		    </condition>
		</template>
	</category>

    <!-- "TEST condition TYPE2 VARIANT2 NO MATCH" / "" -->
	<category>
		<pattern>TEST CONDITION TYPE2 VARIANT2 NO MATCH</pattern>
		<template>
		    <think><set name="var1">XXX</set></think>
		    <condition name="var1">
		        <li> <value>value1</value>X</li>
		        <li> <value>value2</value>Y</li>
		        <li> <value>value3</value>Z</li>
		    </condition>
		</template>
	</category>

    <!-- "TEST condition TYPE3 VARIANT1 NO DEFAULT" / "A" -->
	<category>
		<pattern>TEST CONDITION TYPE3 VARIANT1 NO DEFAULT</pattern>
		<template>
		    <think><set name="var1">value2</set></think>
            <condition>
                <li name="var1" value="value2">A</li>
                <li value="value2"><name>var2</name>B</li>
                <li name="var3"><value>value3</value>C</li>
                <li><name>var4</name><value>value4</value>D</li>
            </condition>
		</template>
	</category>

    <!-- "TEST condition TYPE3 VARIANT1 WITH DEFAULT" / "DEF" -->
	<category>
		<pattern>TEST CONDITION TYPE3 VARIANT1 WITH DEFAULT</pattern>
		<template>
		    <think><set name="var1">XXX</set></think>
            <condition>
                <li name="var1" value="value2">A</li>
                <li value="value2"><name>var2</name>B</li>
                <li name="var3"><value>value3</value>C</li>
                <li><name>var4</name><value>value4</value>D</li>
                <li>DEF</li>
            </condition>
		</template>
	</category>

    <!-- "TEST condition TYPE3 VARIANT1 NO MATCH" / "" -->
	<category>
		<pattern>TEST CONDITION TYPE3 VARIANT1 NO MATCH</pattern>
		<template>
		    <think><set name="var1">XXX</set></think>
            <condition>
                <li name="var1" value="value2">A</li>
                <li value="value2"><name>var2</name>B</li>
                <li name="var3"><value>value3</value>C</li>
                <li><name>var4</name><value>value4</value>D</li>
            </condition>
		</template>
	</category>
    <!-- end from -->


    <!-- AIML 2.x -->

    <!-- "test condition 1 BOUND pred" / "Cpt Jack Sparrow" -->
    <category>
        <pattern>TEST CONDITION 1 BOUND PRED</pattern>
        <template>
            <think><set name="jack">Sparrow</set></think>
            Cpt
            <condition name="jack" value="*">Jack</condition>
            Sparrow
        </template>
    </category>

    <!-- "test condition 1 notbound pred" / "Cpt Sparrow" -->
    <category>
        <pattern>TEST CONDITION 1 NOTBOUND PRED</pattern>
        <template>
            Cpt
            <!-- high chance of unique name :/ -->
            <condition name="kfanoiawfuq92ruq0fd" value="*">Jack</condition>
            Sparrow
        </template>
    </category>

    <!-- "test condition 1 BOUND var" / "Cpt Jack Sparrow" -->
    <category>
        <pattern>TEST CONDITION 1 BOUND VAR</pattern>
        <template>
            <think><set var="jack">Sparrow</set></think>
            Cpt
            <condition var="jack" value="*">Jack</condition>
            Sparrow
        </template>
    </category>

    <!-- "test condition 1 notbound var" / "Cpt Sparrow" -->
    <category>
        <pattern>TEST CONDITION 1 NOTBOUND VAR</pattern>
        <template>
            Cpt
            <!-- high chance of unique name :/ -->
            <condition var="1545454sfefw83ugsvj" value="*">Jack</condition>
            Sparrow
        </template>
    </category>

    <!-- - - - - - - - - - - - - - - - - - - - -->
    <!--                L O O P                -->
    <!-- - - - - - - - - - - - - - - - - - - - -->

    <!-- "test loop miss" / "Y" -->
    <category>
        <pattern>TEST LOOP MISS</pattern>
        <template>
            <set name="var1">value2</set>
            <condition name="var1">
                <li value="value1">X<loop/></li>
		        <li value="value2">Y</li>
		        <li>DEF</li>
            </condition>
        </template>
    </category>

    <!-- "test loop simple" / "A B C" -->
    <category>
        <pattern>TEST LOOP SIMPLE</pattern>
        <template>
            <think>
                <set name="var1">value2</set>
                <set name="var2">value2</set>
            </think>
            <condition>
                <li name="var1" value="value2">
                    <think><set name="var1">value1</set></think>A<loop/>
                </li>
                <li name="var2" value="value2">
                    <think><set name="var2">value1</set></think>B<loop/>
                </li>
		        <li>C</li>
            </condition>
        </template>
    </category>


</aiml>