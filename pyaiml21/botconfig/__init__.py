"""
Bot Configuration.

This module contains definitions of classes used to
store chatbot data.
"""
from .config import BotConfig
from .misc import Properties, Predicates
from .substitutions import Substitutions


__all__ = ["BotConfig", "Predicates", "Properties", "Substitutions"]
