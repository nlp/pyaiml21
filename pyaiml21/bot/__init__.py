"""
The package contains a definition of abstract class `Bot`.

A `Bot` contains necessary methods and attributes needed for interpretation.
"""
from ._bot import Bot

__all__ = ["Bot"]
