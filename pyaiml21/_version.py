"""This module contains the version number of the package."""

__all__ = ["__version__"]

__version__ = "1.0.1.post2"
