"""
Bot-Client Session.

This package contains definition of a ``Session``.

A session is a collection of all the data shared between a chatbot and
a particular user.
"""
from .session import Session

__all__ = ["Session"]
