This file describes the structure of this project:

* `/doc`       - folder with configuration for sphinx, generated documentation lives here
* `/examples`  - a folder with sample aiml conversations
* `/pyaiml21`  - the main package containing the interpreter's source code
* `/scripts`   - scripts distributes with the package
* `/tests`     - folder with tests
* `.gitignore` - ignore files
* `.gitlab-ci.yml` - configuration fo gitlab CI
* `LICENSE`  - file with project's license
* `Makefile` - makefile for creating docs, running test... for more, run `make help`
* `project_structure.md` - this file
* `pyproject.toml` - config for packaging the project, includes mypy config too
* `README.md` - description of the project
* `requirements.txt` - project dependencies
* `requirements-dev.txt` - project dev dependencies (for tests, linters)
* `setup.cfg`   - metadata for packaging this project
* `setup.py`    - script for packaging this project
* `tox.ini`     - used to run tests on different versions of Python
