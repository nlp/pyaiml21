#!/bin/bash


help() {
    echo "Enumerate the files in the first argument"
    echo "Run this script from the bot root examples/"
    exit 1
}


cat ./travel/sets/${1} | tr  -d '\r' | tr '\n' ' '