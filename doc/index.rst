Welcome to pyaiml21 documentation!
==================================

pyaiml21 is a Python 3 package used to create and run a chatbot written in the AIML.


Quick start
-----------

In this tutorial, you will learn how to create a run a simple AIML chatbot
using pyaiml21 interpreters. To learn about AIML and how it works, please refer
to the
`official AIML 2.1 specification <http://www.aiml.foundation/doc.html>`_.


Installation
^^^^^^^^^^^^

First, install the pyaim21 package.

.. code-block:: bash

    $ pip install -U pyaiml21


.. note::
    The AIML specification states, that the bot's knowledge should be stored in
    the *.aiml* files and for AIML 2.1 in interpreter defined files (see section
    below). However, the *pyaiml21* does not include any such files - therefore
    you can not directly run chatbots, you will have to download them, for
    example the standard *AIML 1.0* set from
    `ALICE github repo <https://github.com/drwallace/aiml-en-us-foundation-alice>`_:

    .. code-block:: bash

        $ git clone https://github.com/drwallace/aiml-en-us-foundation-alice.git alice


Running the Interpreter
^^^^^^^^^^^^^^^^^^^^^^^

The easiest way to start the interpreter is via using a command line script.
`pyaiml21` comes with a simple script to run your chatbots, if you unpacked
the contents of your bot into `alice/` folder (so that the structure
is `alice/aiml/..`, `alice/aimlif/..`, `alice/sets/..`), then running the
following will start your bot:

.. code-block:: bash

    $ aiml --bot alice

.. warning::
    Upon loading, the interpreter performs a syntax analysis of given files
    and reports any errors back to the user. The files with syntax errors will
    not be loaded.

.. note::
    For more information on using the script `aiml`, including the file structure
    or the *debugger*, please refer to the `CLI script ./aiml <cli-script.html>`__ or run ``aiml --help``.


It is still possible to start your bot directly from python, e.g.

    >>> from pyaiml21 import Bot
    >>> my_bot = Bot()
    >>> my_bot.learn_aiml("path to .aiml file")
    >>> # to load substitutions, properties or aimlif, see `pyaiml21.utils`

Possible outcome

    >>> my_bot.respond("Hello", "USER_1_ID")
    Hi! How are you doing?


.. note::
    For error reporting, see the documentation to the :py:meth:`Bot.respond`
    method.


For backwards compatibility with the project *pyaiml*, the option to use `Kernel`
object is available:

    >>> from pyaiml21 import Kernel
    >>> kernel = Kernel()

After the bot is loaded, you can use :py:meth:`Bot.respond` method to chat with the bot:

    >>> my_bot.respond("Hello", "my_user_id")
    "Hi."

Alternatively, you can use ``input`` and while loop to run the conversation and accept the user
queries from the command prompt / standard input:

    >>> query = ""
    >>> while query != "QUIT":
    ...     print(my_bot.respond(query, "my_user_id"))
    ...     query = input("> ")


Compatibility
^^^^^^^^^^^^^

`pyaiml21` was created to be compatible with all versions of AIML, namely
AIML 1.0, AIML 1.0.1, AIML 2.0 and AIML 2.1. Furthermore, we tried to preserve
the user interface from a similar project, *pyaiml*, the AIML 1.0 interpreter.

It should be also noted, that the interpreter does not contain processing of
OOB tags and Rich Media Extensions, although their functionality can be added
via extending the interpreter (see below).


Data Storage
^^^^^^^^^^^^

In the basic settings of the ``Bot`` object, the chatbot stores
it's information in the main memory and it is impossible to change this.
However, the ``pyaiml21`` package allows you to store the data into (possibly)
shared database, via classes :py:class:`pyaiml21.graphmaster.SqLiteGraphMaster`
or :py:class:`pyaiml21.graphmaster.MongoGraphMaster`.


Extending the Interpreter with new Tags
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

It is possible to define custom tags for the interpreter, see
`Extensions <extensions.html>`__.


.. toctree::
    :hidden:

    Home Page <self>
    Command Line Script <cli-script>
    Extensions <extensions>


.. autosummary::
   :toctree: _autosummary
   :template: module-template.rst
   :recursive:

   pyaiml21

