PY ?= python
PACKAGE_FOLDER = pyaiml21
REQ_FILE = requirements.txt

SPHINXOPTS ?=
SPHINXBUILD ?= sphinx-build
SPHINX_TARGET = doc

help:
	@echo "Help for makefile of pyaiml21 package:"
	@echo "   make docs      :: generate sphinx documentation from source files in the ${SPHINX_TARGET} folder"
	@echo "   make docstring :: run pydocstyle on docstrings in /${PACKAGE_FOLDER}"
	@echo "   make pypi      :: prepare and upload package onto the PYPI; you will need access token and password"
	@echo "   make install   :: install all dependencies"
	@echo "   make test      :: run tests"
	@echo "   make analysis  :: run static analysis on /${PACKAGE_FOLDER}"
	@echo "   make clean     :: remove created file from 'make pypi'"
	@echo "   make tox       :: run tests on different python versions"


.PHONY: help docs docstring pypi install test analysis clean tox

docs:
	# note that line 42 in stdlib/sets might cause an error (unknown bug), it is enough to replace the definition with {"0"}
	$(PY) -m pip install -r $(SPHINX_TARGET)/$(REQ_FILE)
	cd $(SPHINX_TARGET) && $(SPHINXBUILD) -c . -b html . ./html $(SPHINXOPTS)

docstring: install
	pydocstyle $(PACKAGE_FOLDER)/

pypi: install
	# upgrade or install build
	- $(PY) -m pip install --upgrade build

	# create .whl and .tar.gz archives
	- $(PY) -m build

	# prepare twine
	- $(PY) -m pip install --upgrade twine

	# upload to pypi
	- $(PY) -m twine upload dist/*

install:
	# upgrade or install pip
	- $(PY) -m pip install --upgrade pip

	$(PY) -m pip install -r $(REQ_FILE)
	$(PY) -m pip install -r requirements-dev.txt

test: install
	# to make sure pyaiml21/ is added to path, run it like this
	python -m pytest --doctest-modules

analysis: install
	# ignore flake warnings about spaces around {} - when using inline set, like { 3 }
	flake8 --ignore=E201,E202 $(PACKAGE_FOLDER)/
	mypy $(PACKAGE_FOLDER)/
	pycodestyle $(PACKAGE_FOLDER)/
	pydocstyle $(PACKAGE_FOLDER)/

tox: install tox.ini
	tox

clean:
	# remove build files
	- rm -rf dist/ *egg-info/
